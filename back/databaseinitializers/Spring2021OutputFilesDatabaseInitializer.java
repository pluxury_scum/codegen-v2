package com.gvozdev.codegen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

//@Component
public class Spring2021OutputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Spring2021OutputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/2021/spring/output/";

    @Autowired
    private Spring2021OutputFileRepository repository;

//    @PostConstruct
    @Transactional
    public void init() {
        saveFile(1L, "cpp", "file", "nooop", "main.cpp");
        saveFile(2L, "cpp", "file", "nooop", "mainExample.cpp");
        saveFile(3L, "cpp", "file", "withoop", "main.cpp");
        saveFile(4L, "cpp", "file", "withoop", "mainExample.cpp");
        saveFile(5L, "cpp", "manual", "nooop", "main.cpp");
        saveFile(6L, "cpp", "manual", "nooop", "mainExample.cpp");
        saveFile(7L, "cpp", "manual", "withoop", "main.cpp");
        saveFile(8L, "cpp", "manual", "withoop", "mainExample.cpp");
        saveFile(9L, "java", "file", "withoop", "Main.java");
        saveFile(10L, "java", "file", "withoop", "GraphDrawer.java");
        saveFile(11L, "java", "file", "withoop", "MainExample.java");
        saveFile(12L, "java", "file", "withoop", "GraphDrawerExample.java");
        saveFile(13L, "java", "manual", "withoop", "Main.java");
        saveFile(14L, "java", "manual", "withoop", "GraphDrawer.java");
        saveFile(15L, "java", "manual", "withoop", "MainExample.java");
        saveFile(16L, "java", "manual", "withoop", "GraphDrawerExample.java");
        saveFile(17L, "python", "file", "nooop", "main.py");
        saveFile(18L, "python", "file", "nooop", "mainExample.py");
        saveFile(19L, "python", "file", "withoop", "main.py");
        saveFile(20L, "python", "file", "withoop", "mainExample.py");
        saveFile(21L, "python", "manual", "nooop", "main.py");
        saveFile(22L, "python", "manual", "nooop", "mainExample.py");
        saveFile(23L, "python", "manual", "withoop", "main.py");
        saveFile(24L, "python", "manual", "withoop", "mainExample.py");
        LOGGER.info("Все выходные файлы для УИРС весны 2021 загружены");
    }

    // ===================================================================================================================
    // = Implementation
    // ===================================================================================================================

    private void saveFile(Long id, String lang, String file, String oop, String name) {
        try {
            String filePath = PATH.concat("/")
                                  .concat(lang).concat("/")
                                  .concat(file).concat("/")
                                  .concat(oop).concat("/")
                                  .concat(name);
            File f = new File(filePath);
            byte[] fileBytes = Files.readAllBytes(f.toPath());
            Spring2021OutputFile inputFile = new Spring2021OutputFile(
                id, lang, file, oop, name, fileBytes
            );
            repository.save(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
