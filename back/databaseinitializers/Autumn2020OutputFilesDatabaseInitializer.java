package com.gvozdev.codegen;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.repo.OutputFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class Autumn2020OutputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Autumn2020OutputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/2020/autumn/output/";

    @Autowired
    private OutputFileRepository repository;

    @PostConstruct
    @Transactional
    public void init() {
        saveFile(1L, "var1", "cpp", "file", "nooop", "main.cpp");
        saveFile(2L, "var1", "cpp", "file", "nooop", "mainExample.cpp");
        saveFile(3L, "var1", "cpp", "file", "withoop", "main.cpp");
        saveFile(4L, "var1", "cpp", "file", "withoop", "mainExample.cpp");
        saveFile(5L, "var1", "cpp", "manual", "nooop", "main.cpp");
        saveFile(6L, "var1", "cpp", "manual", "nooop", "mainExample.cpp");
        saveFile(7L, "var1", "cpp", "manual", "withoop", "main.cpp");
        saveFile(8L, "var1", "cpp", "manual", "withoop", "mainExample.cpp");
        saveFile(9L, "var1", "java", "file", "withoop", "Main.java");
        saveFile(10L, "var1", "java", "file", "withoop", "GraphDrawer.java");
        saveFile(11L, "var1", "java", "file", "withoop", "MainExample.java");
        saveFile(12L, "var1", "java", "file", "withoop", "GraphDrawerExample.java");
        saveFile(13L, "var1", "java", "manual", "withoop", "Main.java");
        saveFile(14L, "var1", "java", "manual", "withoop", "GraphDrawer.java");
        saveFile(15L, "var1", "java", "manual", "withoop", "MainExample.java");
        saveFile(16L, "var1", "java", "manual", "withoop", "GraphDrawerExample.java");
        saveFile(17L, "var1", "python", "file", "nooop", "main.py");
        saveFile(18L, "var1", "python", "file", "nooop", "mainExample.py");
        saveFile(19L, "var1", "python", "file", "withoop", "main.py");
        saveFile(20L, "var1", "python", "file", "withoop", "mainExample.py");
        saveFile(21L, "var1", "python", "manual", "nooop", "main.py");
        saveFile(22L, "var1", "python", "manual", "nooop", "mainExample.py");
        saveFile(23L, "var1", "python", "manual", "withoop", "main.py");
        saveFile(24L, "var1", "python", "manual", "withoop", "mainExample.py");
        saveFile(25L, "var2", "cpp", "file", "nooop", "main.cpp");
        saveFile(26L, "var2", "cpp", "file", "nooop", "mainExample.cpp");
        saveFile(27L, "var2", "cpp", "file", "withoop", "main.cpp");
        saveFile(28L, "var2", "cpp", "file", "withoop", "mainExample.cpp");
        saveFile(29L, "var2", "cpp", "manual", "nooop", "main.cpp");
        saveFile(30L, "var2", "cpp", "manual", "nooop", "mainExample.cpp");
        saveFile(31L, "var2", "cpp", "manual", "withoop", "main.cpp");
        saveFile(32L, "var2", "cpp", "manual", "withoop", "mainExample.cpp");
        saveFile(33L, "var2", "java", "file", "withoop", "Main.java");
        saveFile(34L, "var2", "java", "file", "withoop", "GraphDrawer.java");
        saveFile(35L, "var2", "java", "file", "withoop", "MainExample.java");
        saveFile(36L, "var2", "java", "file", "withoop", "GraphDrawerExample.java");
        saveFile(37L, "var2", "java", "manual", "withoop", "Main.java");
        saveFile(38L, "var2", "java", "manual", "withoop", "MainExample.java");
        saveFile(39L, "var2", "java", "manual", "withoop", "GraphDrawer.java");
        saveFile(40L, "var2", "java", "manual", "withoop", "GraphDrawerExample.java");
        saveFile(41L, "var2", "python", "file", "nooop", "main.py");
        saveFile(42L, "var2", "python", "file", "nooop", "mainExample.py");
        saveFile(43L, "var2", "python", "file", "withoop", "main.py");
        saveFile(44L, "var2", "python", "file", "withoop", "mainExample.py");
        saveFile(45L, "var2", "python", "manual", "nooop", "main.py");
        saveFile(46L, "var2", "python", "manual", "nooop", "mainExample.py");
        saveFile(47L, "var2", "python", "manual", "withoop", "main.py");
        saveFile(48L, "var2", "python", "manual", "withoop", "mainExample.py");
        saveFile(49L, "var3", "cpp", "file", "nooop", "main.cpp");
        saveFile(50L, "var3", "cpp", "file", "nooop", "mainExample.cpp");
        saveFile(51L, "var3", "cpp", "file", "withoop", "main.cpp");
        saveFile(52L, "var3", "cpp", "file", "withoop", "mainExample.cpp");
        saveFile(53L, "var3", "cpp", "manual", "nooop", "main.cpp");
        saveFile(54L, "var3", "cpp", "manual", "nooop", "mainExample.cpp");
        saveFile(55L, "var3", "cpp", "manual", "withoop", "main.cpp");
        saveFile(56L, "var3", "cpp", "manual", "withoop", "mainExample.cpp");
        saveFile(57L, "var3", "java", "file", "withoop", "Main.java");
        saveFile(58L, "var3", "java", "file", "withoop", "GraphDrawer.java");
        saveFile(59L, "var3", "java", "file", "withoop", "MainExample.java");
        saveFile(60L, "var3", "java", "file", "withoop", "GraphDrawerExample.java");
        saveFile(61L, "var3", "java", "manual", "withoop", "Main.java");
        saveFile(62L, "var3", "java", "manual", "withoop", "GraphDrawer.java");
        saveFile(63L, "var3", "java", "manual", "withoop", "MainExample.java");
        saveFile(64L, "var3", "java", "manual", "withoop", "GraphDrawerExample.java");
        saveFile(65L, "var3", "python", "file", "nooop", "main.py");
        saveFile(66L, "var3", "python", "file", "nooop", "mainExample.py");
        saveFile(67L, "var3", "python", "file", "withoop", "main.py");
        saveFile(68L, "var3", "python", "file", "withoop", "mainExample.py");
        saveFile(69L, "var3", "python", "manual", "nooop", "main.py");
        saveFile(70L, "var3", "python", "manual", "nooop", "mainExample.py");
        saveFile(71L, "var3", "python", "manual", "withoop", "main.py");
        saveFile(72L, "var3", "python", "manual", "withoop", "mainExample.py");
        LOGGER.info("Все выходные файлы для УИРС осени 2020 загружены");
    }

    // ===================================================================================================================
    // = Implementation
    // ===================================================================================================================

    private void saveFile(Long id, String var, String lang, String file, String oop, String name) {
        try {
            String filePath = PATH.concat("/")
                                  .concat(var).concat("/")
                                  .concat(lang).concat("/")
                                  .concat(file).concat("/")
                                  .concat(oop).concat("/")
                                  .concat(name);
            File f = new File(filePath);
            byte[] fileBytes = Files.readAllBytes(f.toPath());
            OutputFile outputFile = new OutputFile(
                id, var, lang, file, oop, name, fileBytes
            );
            repository.save(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
