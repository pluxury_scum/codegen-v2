package com.gvozdev.codegen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

//@Component
public class Spring2021InputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Spring2021InputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/2021/spring/input/";

    @Autowired
    private Spring2021InputFileRepository repository;

//    @PostConstruct
    @Transactional
    public void init() {
        saveFile(1L, "arti_6hours.dat");
        saveFile(2L, "bshm_1-10.dat");
        saveFile(3L, "gele_6hours.dat");
        saveFile(4L, "hueg_1-10.dat");
        saveFile(5L, "irkm_6hours.dat");
        saveFile(6L, "kslv_6hours.dat");
        saveFile(7L, "leij_1-10.dat");
        saveFile(8L, "maga_6hours.dat");
        saveFile(9L, "menp_6hours.dat");
        saveFile(10L, "novs_6hours.dat");
        saveFile(11L, "noyb_6hours.dat");
        saveFile(12L, "onsa_1-10.dat");
        saveFile(13L, "spt0_1-10.dat");
        saveFile(14L, "svet_6hours.dat");
        saveFile(15L, "svtl_1-10.dat");
        saveFile(16L, "tit2_1-10.dat");
        saveFile(17L, "vis0_1-10.dat");
        saveFile(18L, "vlad_6hours.dat");
        saveFile(19L, "warn_1-10.dat");
        saveFile(20L, "ykts_6hours.dat");
        saveFile(21L, "yusa_6hours.dat");
        saveFile(22L, "zeck_1-10.dat");
        saveFile(23L, "resources.rar");
        LOGGER.info("Все входные файлы для УИРС весны 2021 загружены");
    }

    // ===================================================================================================================
    // = Implementation
    // ===================================================================================================================

    private void saveFile(Long id, String fileName) {
        try {
            File file = new File(PATH + fileName);
            byte[] fileBytes = Files.readAllBytes(file.toPath());
            Spring2021InputFile inputFile = new Spring2021InputFile(id, fileName, fileBytes);
            repository.save(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
