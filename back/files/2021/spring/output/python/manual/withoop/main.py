import matplotlib.pyplot as plt
import numpy as np
from abc import ABC, abstractmethod


class DelayComponent(ABC):
    @abstractmethod
    def getValues(self) -> list:
        pass


class DryComponentFunction(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class VerticalDryComponent(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class WetComponentFunction(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class VerticalWetComponent(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class TroposphericDelays:
    def __init__(self, dryComponentFunction: DelayComponent, verticalDryComponent: DelayComponent,
                 wetComponentFunction: DelayComponent, verticalWetComponent: DelayComponent,
                 amountOfObservations: int) -> None:
        self.__dryComponentFunction: DelayComponent = dryComponentFunction
        self.__verticalDryComponent: DelayComponent = verticalDryComponent
        self.__wetComponentFunction: DelayComponent = wetComponentFunction
        self.__verticalWetComponent: DelayComponent = verticalWetComponent
        self.__amountOfObservations: int = amountOfObservations

    def getDelays(self) -> list:
        dryComponentFunctionValues: list = self.__dryComponentFunction.getValues()
        verticalDryComponentValues: list = self.__verticalDryComponent.getValues()
        wetComponentFunctionValues: list = self.__wetComponentFunction.getValues()
        verticalWetComponentValues: list = self.__verticalWetComponent.getValues()
        delays: list = []
        for observation in range(self.__amountOfObservations):
            md: float = dryComponentFunctionValues[observation]
            td: float = verticalDryComponentValues[observation]
            mw: float = wetComponentFunctionValues[observation]
            tw: float = verticalWetComponentValues[observation]
            delay: float = md * td + mw * tw
            delays.append(delay)
        return delays


class ElevationAngles:
    def __init__(self, degrees: list) -> None:
        self.__degrees: list = degrees

    def getAnglesInHalfCircles(self) -> list:
        halfCircle: float = 180
        halfCircles: list = []
        for angleInDegrees in self.__degrees:
            angleInHalfCircles: float = angleInDegrees / halfCircle
            halfCircles.append(angleInHalfCircles)
        return halfCircles


class Satellite:
    def __init__(self, number: int, troposphericDelays: TroposphericDelays, elevationAngles: ElevationAngles) -> None:
        self.__number = number
        self.__troposphericDelays: TroposphericDelays = troposphericDelays
        self.__elevationAngles: ElevationAngles = elevationAngles

    @property
    def number(self) -> int:
        return self.__number

    @property
    def troposphericDelays(self) -> list:
        delays: list = self.__troposphericDelays.getDelays()
        return delays

    @property
    def elevationAngles(self) -> list:
        angles: list = self.__elevationAngles.getAnglesInHalfCircles()
        return angles


class SatelliteFactory:
    def __init__(self, amountOfObservations: int) -> None:
        self.__amountOfObservations: int = amountOfObservations

    def createSatellite(self, satelliteNumber: int, mdArray: list, tdArray: list, mwArray: list, twArray: list,
                        elevationArray: list) -> Satellite:
        dryComponentFunction: DelayComponent = DryComponentFunction(mdArray)
        verticalDryComponent: DelayComponent = VerticalDryComponent(tdArray)
        wetComponentFunction: DelayComponent = WetComponentFunction(mwArray)
        verticalWetComponent: DelayComponent = VerticalWetComponent(twArray)
        delay: TroposphericDelays = TroposphericDelays(dryComponentFunction, verticalDryComponent,
                                                       wetComponentFunction, verticalWetComponent,
                                                       self.__amountOfObservations)
        angles: ElevationAngles = ElevationAngles(elevationArray)
        satellite: Satellite = Satellite(satelliteNumber, delay, angles)
        return satellite


class ConsoleOutput:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def printInfo(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        satellite1TroposphericDelays: list = self.__satellite1.troposphericDelays
        satellite2TroposphericDelays: list = self.__satellite2.troposphericDelays
        satellite3TroposphericDelays: list = self.__satellite3.troposphericDelays
        satellite1ElevationAngles: list = self.__satellite1.elevationAngles
        satellite2ElevationAngles: list = self.__satellite2.elevationAngles
        satellite3ElevationAngles: list = self.__satellite3.elevationAngles
        print("Спутник #" + str(satellite1Number) + "\t\t\t\t\t\t\t" +
              "Спутник #" + str(satellite2Number) + "\t\t\t\t\t\t\t" +
              "Спутник #" + str(satellite3Number))
        for observation in range(self.__amountOfObservations):
            satellite1TroposphericDelay: float = satellite1TroposphericDelays[observation]
            satellite1ElevationAngle: float = satellite1ElevationAngles[observation]
            satellite2TroposphericDelay: float = satellite2TroposphericDelays[observation]
            satellite2ElevationAngle: float = satellite2ElevationAngles[observation]
            satellite3TroposphericDelay: float = satellite3TroposphericDelays[observation]
            satellite3ElevationAngle: float = satellite3ElevationAngles[observation]
            print(str(round(satellite1TroposphericDelay, 10)) + "\t" + str(round(satellite1ElevationAngle, 10)) +
                  "\t\t" +
                  str(round(satellite2TroposphericDelay, 10)) + "\t" + str(round(satellite2ElevationAngle, 10)) +
                  "\t\t" +
                  str(round(satellite3TroposphericDelay, 10)) + "\t" + str(round(satellite3ElevationAngle, 10)))


class DrawerTemplate(ABC):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def draw(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        measurements1: list = self._getSatellite1Measurements()
        measurements2: list = self._getSatellite2Measurements()
        measurements3: list = self._getSatellite3Measurements()
        yLabel: str = self._getYLabel()
        observations = np.arange(0, self.__amountOfObservations)
        plt.plot(observations, measurements1, 'o-', label="Спутник #" + str(satellite1Number))
        plt.plot(observations, measurements2, 'o-', label="Спутник #" + str(satellite2Number))
        plt.plot(observations, measurements3, 'o-', label="Спутник #" + str(satellite3Number))
        plt.xlabel("Время")
        plt.ylabel(yLabel)
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()

    @abstractmethod
    def _getYLabel(self) -> str:
        pass

    @abstractmethod
    def _getSatellite1Measurements(self) -> list:
        pass

    @abstractmethod
    def _getSatellite2Measurements(self) -> list:
        pass

    @abstractmethod
    def _getSatellite3Measurements(self) -> list:
        pass


class TroposphericDelayDrawer(DrawerTemplate):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(satellite1, satellite2, satellite3, amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def _getYLabel(self) -> str:
        return "Тропосферная задержка сигнала, метры"

    def _getSatellite1Measurements(self) -> list:
        return self.__satellite1.troposphericDelays

    def _getSatellite2Measurements(self) -> list:
        return self.__satellite2.troposphericDelays

    def _getSatellite3Measurements(self) -> list:
        return self.__satellite3.troposphericDelays


class ElevationAnglesDrawer(DrawerTemplate):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(satellite1, satellite2, satellite3, amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def _getYLabel(self) -> str:
        return "Угол возвышения спутника, полуциклы"

    def _getSatellite1Measurements(self) -> list:
        return self.__satellite1.elevationAngles

    def _getSatellite2Measurements(self) -> list:
        return self.__satellite2.elevationAngles

    def _getSatellite3Measurements(self) -> list:
        return self.__satellite3.elevationAngles


def main():
    amountOfObservations: int = 360

    satellite1Number: int = ?
    satellite2Number: int = ?
    satellite3Number: int = ?

    satellite1Md: list = [ ? ]

    satellite1Td: list = [ ? ]

    satellite1Mw: list = [ ? ]

    satellite1Tw: list = [ ? ]

    satellite2Md: list = [ ? ]

    satellite2Td: list = [ ? ]

    satellite2Mw: list = [ ? ]

    satellite2Tw: list = [ ? ]

    satellite3Md: list = [ ? ]

    satellite3Td: list = [ ? ]

    satellite3Mw: list = [ ? ]

    satellite3Tw: list = [ ? ]

    satellite1Elevation: list = [ ? ]

    satellite2Elevation: list = [ ? ]

    satellite3Elevation: list = [ ? ]

    satelliteFactory: SatelliteFactory = SatelliteFactory(amountOfObservations)

    satellite1: Satellite = satelliteFactory.createSatellite(satellite1Number, satellite1Md, satellite1Td, satellite1Mw,
                                                             satellite1Tw, satellite1Elevation)

    satellite2: Satellite = satelliteFactory.createSatellite(satellite2Number, satellite2Md, satellite2Td, satellite2Mw,
                                                             satellite2Tw, satellite2Elevation)

    satellite3: Satellite = satelliteFactory.createSatellite(satellite3Number, satellite3Md, satellite3Td, satellite3Mw,
                                                             satellite3Tw, satellite3Elevation)

    consoleOutput: ConsoleOutput = ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations)
    consoleOutput.printInfo()

    troposphericDelayDrawer: DrawerTemplate = TroposphericDelayDrawer(satellite1, satellite2, satellite3,
                                                                      amountOfObservations)
    troposphericDelayDrawer.draw()

    elevationAnglesDrawer: DrawerTemplate = ElevationAnglesDrawer(satellite1, satellite2, satellite3,
                                                                  amountOfObservations)
    elevationAnglesDrawer.draw()


if __name__ == '__main__':
    main()
