import matplotlib.pyplot as plt
import numpy as np


def getTroposphericDelays(dryComponentFunctionValues: list, verticalDryComponentValues: list,
                          wetComponentFunctionValues: list, verticalWetComponentValues: list,
                          amountOfObservations: int) -> list:
    delays: list = []
    for observation in range(amountOfObservations):
        md: float = dryComponentFunctionValues[observation]
        td: float = verticalDryComponentValues[observation]
        mw: float = wetComponentFunctionValues[observation]
        tw: float = verticalWetComponentValues[observation]
        delay: float = md * td + mw * tw
        delays.append(delay)
    return delays


def getAnglesInHalfCircles(degrees: list) -> list:
    halfCircle: float = 180
    halfCircles: list = []
    for angleInDegrees in degrees:
        angleInHalfCircles: float = angleInDegrees / halfCircle
        halfCircles.append(angleInHalfCircles)
    return halfCircles


def printInfo(satellite1Number: int, satellite2Number: int, satellite3Number: int,
              satellite1TroposphericDelays: list, satellite2TroposphericDelays: list,
              satellite3TroposphericDelays: list, satellite1ElevationAngles: list, satellite2ElevationAngles: list,
              satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    print("Спутник #" + str(satellite1Number) + "\t\t\t\t\t\t\t" +
          "Спутник #" + str(satellite2Number) + "\t\t\t\t\t\t\t" +
          "Спутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        satellite1TroposphericDelay: float = satellite1TroposphericDelays[observation]
        satellite1ElevationAngle: float = satellite1ElevationAngles[observation]
        satellite2TroposphericDelay: float = satellite2TroposphericDelays[observation]
        satellite2ElevationAngle: float = satellite2ElevationAngles[observation]
        satellite3TroposphericDelay: float = satellite3TroposphericDelays[observation]
        satellite3ElevationAngle: float = satellite3ElevationAngles[observation]
        print(str(round(satellite1TroposphericDelay, 10)) + "\t" + str(round(satellite1ElevationAngle, 10)) + "\t\t" +
              str(round(satellite2TroposphericDelay, 10)) + "\t" + str(round(satellite2ElevationAngle, 10)) + "\t\t" +
              str(round(satellite3TroposphericDelay, 10)) + "\t" + str(round(satellite3ElevationAngle, 10)) + "\t\t")


def drawTroposphericDelays(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                           satellite1TroposphericDelays: list, satellite2TroposphericDelays: list,
                           satellite3TroposphericDelays: list, amountOfObservations: int) -> None:
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations, satellite1TroposphericDelays, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2TroposphericDelays, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3TroposphericDelays, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Тропосферная задержка сигнала, метры")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def drawElevationAngles(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                        satellite1ElevationAngles: list, satellite2ElevationAngles: list,
                        satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations, satellite1ElevationAngles, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2ElevationAngles, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3ElevationAngles, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Угол возвышения спутника, полуциклы")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 360

    satellite1Number: int = ?
    satellite2Number: int = ?
    satellite3Number: int = ?

    satellite1Md: list = [ ? ]

    satellite1Td: list = [ ? ]

    satellite1Mw: list = [ ? ]

    satellite1Tw: list = [ ? ]

    satellite2Md: list = [ ? ]

    satellite2Td: list = [ ? ]

    satellite2Mw: list = [ ? ]

    satellite2Tw: list = [ ? ]

    satellite3Md: list = [ ? ]

    satellite3Td: list = [ ? ]

    satellite3Mw: list = [ ? ]

    satellite3Tw: list = [ ? ]

    satellite1Elevation: list = [ ? ]

    satellite2Elevation: list = [ ? ]

    satellite3Elevation: list = [ ? ]

    satellite1ElevationAngles: list = getAnglesInHalfCircles(satellite1Elevation)
    satellite2ElevationAngles: list = getAnglesInHalfCircles(satellite2Elevation)
    satellite3ElevationAngles: list = getAnglesInHalfCircles(satellite3Elevation)

    satellite1TroposphericDelays: list = getTroposphericDelays(satellite1Md, satellite1Td, satellite1Mw, satellite1Tw,
                                                               amountOfObservations)

    satellite2TroposphericDelays: list = getTroposphericDelays(satellite2Md, satellite2Td, satellite2Mw, satellite2Tw,
                                                               amountOfObservations)

    satellite3TroposphericDelays: list = getTroposphericDelays(satellite3Md, satellite3Td, satellite3Mw, satellite3Tw,
                                                               amountOfObservations)

    printInfo(satellite1Number, satellite2Number, satellite3Number,
              satellite1TroposphericDelays, satellite2TroposphericDelays, satellite3TroposphericDelays,
              satellite1ElevationAngles, satellite2ElevationAngles,
              satellite3ElevationAngles, amountOfObservations)

    drawTroposphericDelays(satellite1Number, satellite2Number, satellite3Number,
                           satellite1TroposphericDelays, satellite2TroposphericDelays, satellite3TroposphericDelays,
                           amountOfObservations)

    drawElevationAngles(satellite1Number, satellite2Number, satellite3Number,
                        satellite1ElevationAngles, satellite2ElevationAngles,
                        satellite3ElevationAngles, amountOfObservations)


if __name__ == '__main__':
    main()
