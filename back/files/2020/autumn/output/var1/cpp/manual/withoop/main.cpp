﻿#include <iostream>
#include <vector>
#include <fstream>
#include <iomanip>

class PseudoRange {
public:
    PseudoRange(std::vector<double> pseudoRanges, int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double pseudoRange = pseudoRanges.at(observation);
            this->pseudoRanges.push_back(pseudoRange);
        }
    }

    double getPseudoRangeAt(int observation) {
        return pseudoRanges.at(observation);
    }

private:
    int amountOfObservations;
    std::vector<double> pseudoRanges;
};

class IonosphericDelay {
public:
    IonosphericDelay(PseudoRange& pseudoRange1, PseudoRange& pseudoRange2) {
        this->pseudoRange1 = &pseudoRange1;
        this->pseudoRange2 = &pseudoRange2;
    }

    double getIonosphericDelayAt(int observation) {
        double speedOfLight = 2.99792458 * 1E8;
        double p1 = pseudoRange1->getPseudoRangeAt(observation);
        double p2 = pseudoRange2->getPseudoRangeAt(observation);
        double k = getK();
        double delay = (p1 - p2) / (speedOfLight * (1 - k));
        double delayInMeters = delay * speedOfLight;
        return delayInMeters;
    }

private:
    PseudoRange* pseudoRange1;
    PseudoRange* pseudoRange2;

    double getK() {
        double f1 = 1575420000;
        double f2 = 1227600000;
        double k = pow(f1, 2) / pow(f2, 2);
        return k;
    }
};

class Satellite {
public:
    Satellite(int number, IonosphericDelay& ionosphericDelay) {
        this->number = number;
        this->ionosphericDelay = &ionosphericDelay;
    }

    int getNumber() {
        return number;
    }

    double getIonosphericDelayAt(int observation) {
        return ionosphericDelay->getIonosphericDelayAt(observation);
    }

private:
    int number;
    IonosphericDelay* ionosphericDelay;
};


class SatelliteFactory {
public:
    Satellite* createSatellite(int number, std::vector<double> p1Array, std::vector<double> p2Array, int amountOfObservations) {
        p1 = new PseudoRange(p1Array, amountOfObservations);
        p2 = new PseudoRange(p2Array, amountOfObservations);
        delay = new IonosphericDelay(*p1, *p2);
        satellite = new Satellite(number, *delay);
        return satellite;
    }

private:
    int number;
    PseudoRange* p1;
    PseudoRange* p2;
    IonosphericDelay* delay;
    Satellite* satellite;
};


class ConsoleOutput {
public:
    ConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
        this->amountOfObservations = amountOfObservations;
    }

    void printDelays() {
        int satellite1Number = satellite1->getNumber();
        int satellite2Number = satellite2->getNumber();
        int satellite3Number = satellite3->getNumber();
        std::cout << "Ионосферная задержка\nСпутник #" << satellite1Number << "\t\tСпутник#" << satellite2Number <<
            "\t\tСпутник #" << satellite3Number << std::endl;
        std::cout << std::fixed << std::setprecision(10);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double delay1 = satellite1->getIonosphericDelayAt(observation);
            double delay2 = satellite2->getIonosphericDelayAt(observation);
            double delay3 = satellite3->getIonosphericDelayAt(observation);
            std::cout << delay1 << "\t" << delay2 << "\t" << delay3 << std::endl;
        }
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
    int amountOfObservations;
};


int main() {
    setlocale(LC_ALL, "rus");
    const int amountOfObservations = 360;

    std::vector<double> satellite1P1 { ? };

    std::vector<double> satellite1P2 { ? };

    std::vector<double> satellite2P1 { ? };

    std::vector<double> satellite2P2 { ? };

    std::vector<double> satellite3P1 { ? };

    std::vector<double> satellite3P2 { ? };

    SatelliteFactory satelliteFactory;
    Satellite* satellite1 = satelliteFactory.createSatellite(?, satellite1P1, satellite1P2, amountOfObservations);
    Satellite* satellite2 = satelliteFactory.createSatellite(?, satellite2P1, satellite2P2, amountOfObservations);
    Satellite* satellite3 = satelliteFactory.createSatellite(?, satellite3P1, satellite3P2, amountOfObservations);

    ConsoleOutput consoleOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
    consoleOutput.printDelays();
}