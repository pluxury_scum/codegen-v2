package com.gvozdev.codegen.spring2021.unittests;

import com.gvozdev.codegen.spring2021.domain.FileReader;
import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.service.InputFileServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SatelliteNumbersTests {

    @Autowired
    private InputFileServiceImpl service;

    @Test
    void shouldGetSatelliteNumbers() {
        InputFile inputFile = service.findByName("arti_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes);
        List<Integer> expected = asList(
            3, 6, 9, 16, 22, 23, 26, 31
        );
        List<Integer> actual = fileReader.getSatelliteNumbers();
        assertEquals(expected, actual);
    }
}
