package com.gvozdev.codegen.autumn2020.filestests.outputfiles;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Var2CppFileWithOopFilesLengthTests {
    private final String var = "var2";
    private final String lang = "cpp";
    private final String file = "file";
    private final String oop = "withoop";

    @Autowired
    private OutputFileService service;

    @Test
    void mainLengthInBytes() {
        String fileName = "main.cpp";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 21497;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void mainExampleLengthInBytes() {
        String fileName = "mainExample.cpp";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 21549;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}
