package com.gvozdev.codegen.spring2021.filestests.outputfiles;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class JavaFileWithOopFilesLengthTests {
    private final String lang = "java";
    private final String file = "file";
    private final String oop = "withoop";

    @Autowired
    private OutputFileService service;

    @Test
    void shouldCheckMainLengthInBytes() {
        String fileName = "Main.java";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 16733;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckGraphDrawerLengthInBytes() {
        String fileName = "GraphDrawer.java";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 5834;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckMainExampleLengthInBytes() {
        String fileName = "MainExample.java";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 16753;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckGraphDrawerExampleLengthInBytes() {
        String fileName = "GraphDrawerExample.java";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 5854;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}
