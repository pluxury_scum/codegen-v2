package com.gvozdev.codegen.spring2021.domain;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class FileService {
    private final FileReader fileReader;

    public FileService(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    public List<Satellite> getSatellites() {
        List<Integer> satelliteNumbers = extractSatelliteNumbers();
        int satellite1Number = satelliteNumbers.get(0);
        int satellite2Number = satelliteNumbers.get(1);
        int satellite3Number = satelliteNumbers.get(2);

        List<List<List<Double>>> troposphericDelayComponents = extractTroposphericDelayComponents(satelliteNumbers);

        List<Double> satellite1Md = troposphericDelayComponents.get(0).get(0);
        List<Double> satellite1Td = troposphericDelayComponents.get(0).get(1);
        List<Double> satellite1Mw = troposphericDelayComponents.get(0).get(2);
        List<Double> satellite1Tw = troposphericDelayComponents.get(0).get(3);

        List<Double> satellite2Md = troposphericDelayComponents.get(1).get(0);
        List<Double> satellite2Td = troposphericDelayComponents.get(1).get(1);
        List<Double> satellite2Mw = troposphericDelayComponents.get(1).get(2);
        List<Double> satellite2Tw = troposphericDelayComponents.get(1).get(3);

        List<Double> satellite3Md = troposphericDelayComponents.get(2).get(0);
        List<Double> satellite3Td = troposphericDelayComponents.get(2).get(1);
        List<Double> satellite3Mw = troposphericDelayComponents.get(2).get(2);
        List<Double> satellite3Tw = troposphericDelayComponents.get(2).get(3);

        List<List<Double>> elevationAngles = extractElevationAngles(satelliteNumbers);

        List<Double> satellite1Elevation = elevationAngles.get(0);
        List<Double> satellite2Elevation = elevationAngles.get(1);
        List<Double> satellite3Elevation = elevationAngles.get(2);

        Satellite satellite1 = createSatellite(
            satellite1Number,
            satellite1Md, satellite1Td,
            satellite1Mw, satellite1Tw,
            satellite1Elevation
        );

        Satellite satellite2 = createSatellite(
            satellite2Number,
            satellite2Md, satellite2Td,
            satellite2Mw, satellite2Tw,
            satellite2Elevation
        );

        Satellite satellite3 = createSatellite(
            satellite3Number,
            satellite3Md, satellite3Td,
            satellite3Mw, satellite3Tw,
            satellite3Elevation
        );

        return asList(satellite1, satellite2, satellite3);
    }

    // =========================================================================================================================================================
    // Implementation
    // =========================================================================================================================================================

    private Satellite createSatellite(
        int number,
        List<Double> md, List<Double> td,
        List<Double> mw, List<Double> tw,
        List<Double> elevation
    ) {
        return new Satellite(number, md, td, mw, tw, elevation);
    }

    private List<Integer> extractSatelliteNumbers() {
        return fileReader.getSatelliteNumbers();
    }

    private List<List<List<Double>>> extractTroposphericDelayComponents(List<Integer> satelliteNumbers) {
        List<List<List<Double>>> allTroposphericDelayComponents = new ArrayList<>();
        for (int satelliteNumber : satelliteNumbers) {
            List<List<Double>> troposphericDelayComponents;
            char lastDigit = toChar(satelliteNumber % 10);
            if (numberIsDoubleDigit(satelliteNumber)) {
                char firstDigit = toChar(satelliteNumber / 10);
                int requiredSatelliteNumberSize = 2;
                troposphericDelayComponents = fileReader.getTroposphericDelayComponents(
                    firstDigit, lastDigit, requiredSatelliteNumberSize
                );
            } else {
                int requiredSatelliteNumberSize = 1;
                troposphericDelayComponents = fileReader.getTroposphericDelayComponents(
                    lastDigit, requiredSatelliteNumberSize
                );
            }
            allTroposphericDelayComponents.add(troposphericDelayComponents);
        }
        return allTroposphericDelayComponents;
    }

    private List<List<Double>> extractElevationAngles(List<Integer> satelliteNumbers) {
        List<List<Double>> allElevationAngles = new ArrayList<>();
        for (int satelliteNumber : satelliteNumbers) {
            List<Double> elevationAngles;
            char lastDigit = toChar(satelliteNumber % 10);
            if (numberIsDoubleDigit(satelliteNumber)) {
                char firstDigit = toChar(satelliteNumber / 10);
                int requiredSatelliteNumberSize = 2;
                elevationAngles = fileReader.getElevationAngles(
                    firstDigit, lastDigit, requiredSatelliteNumberSize
                );
            } else {
                int requiredSatelliteNumberSize = 1;
                elevationAngles = fileReader.getElevationAngles(
                    lastDigit, requiredSatelliteNumberSize
                );
            }
            allElevationAngles.add(elevationAngles);
        }
        return allElevationAngles;
    }

    private char toChar(int number) {
        return (char)(number + 48);
    }

    private boolean numberIsDoubleDigit(int number) {
        return number > 9;
    }
}
