package com.gvozdev.codegen.spring2021.service;

import com.gvozdev.codegen.spring2021.entity.InputFile;

public interface InputFileService {
    InputFile findByName(String name);
}
