package com.gvozdev.codegen.autumn2020.restadapter.var3;

import com.gvozdev.codegen.autumn2020.domain.var3.Components;
import com.gvozdev.codegen.autumn2020.domain.var3.EphemerisFileReader;
import com.gvozdev.codegen.autumn2020.domain.var3.FileService;
import com.gvozdev.codegen.autumn2020.domain.var3.IonoFileReader;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import org.springframework.web.bind.annotation.*;

@RestController("Autumn2020Var3Controller")
@RequestMapping("/year/2020/autumn/exercise-info/var3")
@CrossOrigin(origins = "http://localhost:3000")
public class PageController {
    private InputFileService service;

    public PageController(InputFileService service) {
        this.service = service;
    }

    @GetMapping(value = "", produces = "application/json")
    public Components getComponents(
        @RequestParam(value = "lat1") String lat1,
        @RequestParam(value = "lat2") String lat2,
        @RequestParam(value = "lon1") String lon1,
        @RequestParam(value = "lon2") String lon2,
        @RequestParam(value = "satnum") String satelliteNumber
    ) {
        InputFile ephemerisFile = service.findByName("brdc0010.18n");
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(ephemerisFileBytes);

        InputFile forecastFile = service.findByName("igrg0010.18i");
        byte[] forecastFileBytes = forecastFile.getFileBytes();
        IonoFileReader forecastIonoFileReader = new IonoFileReader(forecastFileBytes);

        InputFile preciseFile = service.findByName("igsg0010.18i");
        byte[] preciseFileBytes = preciseFile.getFileBytes();
        IonoFileReader preciseIonoFileReader = new IonoFileReader(preciseFileBytes);

        FileService fileService = new FileService(
            ephemerisFileReader, forecastIonoFileReader,
            preciseIonoFileReader, satelliteNumber
        );

        return fileService.getComponents(lat1, lat2, lon1, lon2);
    }
}