package com.gvozdev.codegen.autumn2020.domain.var1;

import java.util.*;

import static java.util.Arrays.asList;

public class FileService {
    private final FileReader reader;

    public FileService(FileReader reader) {
        this.reader = reader;
    }

    public List<Satellite> getSatellites() {
        List<Integer> satelliteNumbers = extractSatelliteNumbers();
        int satellite1Number = satelliteNumbers.get(0);
        int satellite2Number = satelliteNumbers.get(1);
        int satellite3Number = satelliteNumbers.get(2);

        List<List<List<Double>>> pseudoRanges = extractPseudoRanges(satelliteNumbers);
        List<Double> satellite1P1 = pseudoRanges.get(0).get(0);
        List<Double> satellite1P2 = pseudoRanges.get(0).get(1);

        List<Double> satellite2P1 = pseudoRanges.get(1).get(0);
        List<Double> satellite2P2 = pseudoRanges.get(1).get(1);

        List<Double> satellite3P1 = pseudoRanges.get(2).get(0);
        List<Double> satellite3P2 = pseudoRanges.get(2).get(1);

        Satellite satellite1 = createSatellite(satellite1Number, satellite1P1, satellite1P2);
        Satellite satellite2 = createSatellite(satellite2Number, satellite2P1, satellite2P2);
        Satellite satellite3 = createSatellite(satellite3Number, satellite3P1, satellite3P2);

        return asList(satellite1, satellite2, satellite3);
    }

    // =========================================================================================================================================================
    // Implementation
    // =========================================================================================================================================================

    private static Satellite createSatellite(int satelliteNumber, List<Double> p1, List<Double> p2) {
        return new Satellite(satelliteNumber, p1, p2);
    }

    private List<Integer> extractSatelliteNumbers() {
        int fromIndex = 0;
        int toIndex = 3;
        return reader.getSatelliteNumbers().subList(fromIndex, toIndex);
    }

    private List<List<List<Double>>> extractPseudoRanges(List<Integer> satelliteNumbers) {
        List<List<List<Double>>> allPseudoRanges = new ArrayList<>();
        for (int satelliteNumber : satelliteNumbers) {
            List<List<Double>> pseudoRanges;
            char lastDigit = toChar(satelliteNumber % 10);
            if (numberIsDoubleDigit(satelliteNumber)) {
                char firstDigit = toChar(satelliteNumber / 10);
                int satelliteNumberSize = 2;
                pseudoRanges = reader.getPseudoRanges(firstDigit, lastDigit, satelliteNumberSize);
            } else {
                int satelliteNumberSize = 1;
                pseudoRanges = reader.getPseudoRanges(lastDigit, satelliteNumberSize);
            }
            allPseudoRanges.add(pseudoRanges);
        }
        return allPseudoRanges;
    }

    private char toChar(int number) {
        return (char)(number + 48);
    }

    private boolean numberIsDoubleDigit(int number) {
        return number > 9;
    }
}
