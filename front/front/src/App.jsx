import { Switch, Route } from "react-router-dom";
import MainPage from "./pages/main-page/MainPage";
import Autumn2020Landing from "./pages/2020/autumn/landing/Landing";
import Autumn2020ActionsOverview from "./pages/2020/autumn/actions-overview/ActionsOverview";
import Autumn2020ExerciseInfoForm from "./pages/2020/autumn/exercise-info-form/ExerciseInfoForm";
import Autumn2020ExerciseInfoRouter from "./pages/2020/autumn/exercise-info/ExerciseInfoRouter";
import Spring2021Landing from "./pages/2021/spring/landing/Landing";
import Spring2021ActionsOverview from "./pages/2021/spring/actions-overview/ActionsOverview";
import Spring2021ExerciseInfoForm from "./pages/2021/spring/exercise-info-form/ExerciseInfoForm";
import Spring2021FetchExerciseInfo from "./pages/2021/spring/exercise-info/FetchExerciseInfo";

const App = () => {
  return (
    <div>
      <Switch>
        <Route path="/" component={MainPage} exact />
        <Route path="/year/2020/autumn" component={Autumn2020Landing} exact />
        <Route path="/year/2020/autumn/actions-overview" component={Autumn2020ActionsOverview} exact />
        <Route path="/year/2020/autumn/exercise-info-form" component={Autumn2020ExerciseInfoForm} exact />
        <Route path="/year/2020/autumn/exercise-info" component={Autumn2020ExerciseInfoRouter} exact />
        <Route path="/year/2021/spring" component={Spring2021Landing} exact />
        <Route path="/year/2021/spring/actions-overview" component={Spring2021ActionsOverview} exact />
        <Route path="/year/2021/spring/exercise-info-form" component={Spring2021ExerciseInfoForm} exact />
        <Route path="/year/2021/spring/exercise-info" component={Spring2021FetchExerciseInfo} exact />
      </Switch>
    </div>
  );
}

export default App;