export const extractLang = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lang = searchParams.get("lang");
  return lang;
}

export const extractFileParam = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const type = searchParams.get("file") === "true" ? "file" : "manual";
  return type;
}

export const extractOopParam = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const type = searchParams.get("oop") === "true" ? "withoop" : "nooop";
  return type;
}

export const extractFileName = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const fileName = searchParams.get("filename");
  return fileName;
}