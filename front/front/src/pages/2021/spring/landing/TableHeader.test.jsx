import { render, unmountComponentAtNode } from "react-dom";
import TableHeader from "./TableHeader";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFirstHeader", () => {
  render(<TableHeader />, container);

  const actual = container.getElementsByTagName("TH")[1].textContent;
  const expected = "С чтением файла (на 5)";
  expect(actual).toBe(expected);
});

it("shouldRenderSecondHeader", () => {
  render(<TableHeader />, container);

  const actual = container.getElementsByTagName("TH")[2].textContent;
  const expected = "Без чтения файла (на 3/4)";
  expect(actual).toBe(expected);
});