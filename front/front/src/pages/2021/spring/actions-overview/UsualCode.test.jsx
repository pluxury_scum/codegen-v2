import { render, unmountComponentAtNode } from "react-dom";
import UsualCode from "./UsualCode";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadMainButton", () => {
  render(<UsualCode fileName="main.cpp" />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить main.cpp";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonDownloadMainButton", () => {
  render(<UsualCode fileName="main.py" />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить main.py";
  expect(actual).toBe(expected);
});