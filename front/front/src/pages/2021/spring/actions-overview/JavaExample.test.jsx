import { render, unmountComponentAtNode } from "react-dom";
import JavaExample from "./JavaExample";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadMainExampleButton", () => {
  render(<JavaExample />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить MainExample.java";
  expect(actual).toBe(expected);
});

it("shouldRenderDownloadGraphDrawerExampleButton", () => {
  render(<JavaExample />, container);

  const actual = container.getElementsByTagName("BUTTON")[1].textContent;
  const expected = "Загрузить GraphDrawerExample.java";
  expect(actual).toBe(expected);
});