import { render, unmountComponentAtNode } from "react-dom";
import DownloadCodeButton from "./DownloadCodeButton";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadCodeButton", () => {
  render(<DownloadCodeButton lang="cpp" />, container);

  const actual = container.getElementsByTagName("A")[0].textContent;
  const expected = "Загрузить main.cpp";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonDownloadCodeButton", () => {
  render(<DownloadCodeButton lang="python" />, container);

  const actual = container.getElementsByTagName("A")[0].textContent;
  const expected = "Загрузить main.py";
  expect(actual).toBe(expected);
});

it("shouldRenderJavaDownloadCodeButtons", () => {
  render(<DownloadCodeButton lang="java" />, container);

  const actualMain = container.getElementsByTagName("A")[0].textContent;
  const expectedMain = "Загрузить Main.java";

  const actualGraphDrawer = container.getElementsByTagName("A")[1].textContent;
  const expectedGraphDrawer = "Загрузить GraphDrawer.java";

  expect(actualMain).toBe(expectedMain);
  expect(actualGraphDrawer).toBe(expectedGraphDrawer);
});