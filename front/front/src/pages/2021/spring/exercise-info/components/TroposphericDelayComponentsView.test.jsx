import { render, unmountComponentAtNode } from "react-dom";
import TroposphericDelayComponentsView from "./TroposphericDelayComponentsView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTroposphericDelayComponentsView", () => {
  const order = "первого";
  const md = ["1", "2", "3", "4", "5"];
  const td = ["6", "7", "8", "9", "10"];
  const mw = ["11", "12", "13", "14", "15"];
  const tw = ["16", "17", "18", "19", "20"];
  render(
    <TroposphericDelayComponentsView
      order={order}
      md={md}
      td={td}
      mw={mw}
      tw={tw}
    />, container
  );

  const actual = container.textContent;
  const expected = "Компонент md первого спутника:1, 2, 3, 4, 5, " +
    "Компонент td первого спутника:6, 7, 8, 9, 10, " +
    "Компонент mw первого спутника:11, 12, 13, 14, 15, " +
    "Компонент tw первого спутника:16, 17, 18, 19, 20, ";
  expect(actual).toBe(expected);
});