import { useState, useEffect } from "react";
import {
  extractLat, extractLon,
  extractLat1, extractLat2,
  extractLon1, extractLon2,
  extractSatelliteNumber
} from "../../service/queryParamsExtraction";
import getVar3ComponentsData from "../api/getVar3ComponentsData";
import Var3ExerciseInfo from "./Var3ExerciseInfo";

const FetchVar3ExerciseInfo = props => {
  const [componentsData, setComponentsData] = useState({
    gpsTime: {},
    alpha: {},
    beta: {},
    forecastValues: [],
    preciseValues: []
  });
  const [inputData, setInputData] = useState({});
  const lat = extractLat(props.routerSearch);
  const lon = extractLon(props.routerSearch);
  const lat1 = extractLat1(props.routerSearch);
  const lat2 = extractLat2(props.routerSearch);
  const lon1 = extractLon1(props.routerSearch);
  const lon2 = extractLon2(props.routerSearch);
  const satelliteNumber = extractSatelliteNumber(props.routerSearch);


  useEffect(() => {
    getVar3ComponentsData(
      lat, lon, lat1, lat2, lon1, lon2, satelliteNumber
    ).then(componentsData => {
      setComponentsData({
        gpsTime: componentsData.gpsTime,
        alpha: componentsData.alpha,
        beta: componentsData.beta,
        forecastValues: componentsData.forecastValues,
        preciseValues: componentsData.preciseValues
      });
      setInputData({
        lat: lat,
        lon: lon,
        lat1: lat1,
        lat2: lat2,
        lon1: lon1,
        lon2: lon2,
        satelliteNumber: satelliteNumber
      });
    });
  }, [lat, lon, lat1, lat2, lon1, lon2, satelliteNumber]);

  console.log(componentsData);
  return (
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />
  );
}

export default FetchVar3ExerciseInfo;