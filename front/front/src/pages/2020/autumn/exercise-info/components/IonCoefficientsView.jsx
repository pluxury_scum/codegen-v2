import getValuesDividedByComma from "../../service/getValuesDividedByComma";

const IonCoefficientsView = props => {
  const coefficients = getValuesDividedByComma(props.coefficients.coefficients);

  return (
    <div>Массив {props.coefficientsName}-коэффициентов:
      <span className="inf">{coefficients}</span>
    </div>
  );
}

export default IonCoefficientsView;