import { useState, useEffect } from "react";
import { extractFileName, extractVarNumber } from "../../service/queryParamsExtraction";
import getVar2SatellitesData from "../api/getVar2SatellitesData";
import Var2ExerciseInfo from "./Var2ExerciseInfo";

const FetchVar2ExerciseInfo = props => {
  const [satellite1Data, setSatellite1Data] = useState({});
  const [satellite2Data, setSatellite2Data] = useState({});
  const [satellite3Data, setSatellite3Data] = useState({});
  const fileName = extractFileName(props.routerSearch);
  const varNumber = extractVarNumber(props.routerSearch);

  useEffect(() => {
    getVar2SatellitesData(fileName).then(satellitesData => {
      const satellite1 = satellitesData.satellite1;
      setSatellite1Data({
        number: satellite1.number,
        elevation: satellite1.elevation,
      });
      const satellite2 = satellitesData.satellite2;
      setSatellite2Data({
        number: satellite2.number,
        elevation: satellite2.elevation,
      });
      const satellite3 = satellitesData.satellite3;
      setSatellite3Data({
        number: satellite3.number,
        elevation: satellite3.elevation,
      });
    });
  }, [fileName]);

  return (
    <Var2ExerciseInfo
      varNumber={varNumber}
      fileName={fileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />
  );
}

export default FetchVar2ExerciseInfo;