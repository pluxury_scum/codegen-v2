import axios from "axios";

const getVar3ComponentsData = (lat, lon, lat1, lat2, lon1, lon2, satelliteNumber) => {
  const componentsData = axios.get("http://localhost:8080/year/2020/autumn/exercise-info/var3", {
    params: {
      lat: lat,
      lon: lon,
      lat1: lat1,
      lat2: lat2,
      lon1: lon1,
      lon2: lon2,
      satnum: satelliteNumber
    }
  }).then(response => {
    const components = response.data;
    return components;
  });

  return componentsData;
}

export default getVar3ComponentsData