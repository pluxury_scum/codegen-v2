import { useState, useEffect } from "react";
import { extractFileName, extractVarNumber } from "../../service/queryParamsExtraction";
import getVar1SatellitesData from "../api/getVar1SatellitesData";
import Var1ExerciseInfo from "./Var1ExerciseInfo";

const FetchVar1ExerciseInfo = props => {
  const [satellite1Data, setSatellite1Data] = useState({});
  const [satellite2Data, setSatellite2Data] = useState({});
  const [satellite3Data, setSatellite3Data] = useState({});
  const fileName = extractFileName(props.routerSearch);
  const varNumber = extractVarNumber(props.routerSearch);

  useEffect(() => {
    getVar1SatellitesData(fileName).then(satellitesData => {
      const satellite1 = satellitesData.satellite1;
      setSatellite1Data({
        number: satellite1.number,
        p1: satellite1.p1,
        p2: satellite1.p2
      });
      const satellite2 = satellitesData.satellite2;
      setSatellite2Data({
        number: satellite2.number,
        p1: satellite2.p1,
        p2: satellite2.p2
      });
      const satellite3 = satellitesData.satellite3;
      setSatellite3Data({
        number: satellite3.number,
        p1: satellite3.p1,
        p2: satellite3.p2
      });
    });
  }, [fileName]);

  return (
    <Var1ExerciseInfo
      varNumber={varNumber}
      fileName={fileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />
  );
}

export default FetchVar1ExerciseInfo;