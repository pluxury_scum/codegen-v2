import { render, unmountComponentAtNode } from "react-dom";
import Var1ExerciseInfo from "./Var1ExerciseInfo";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderExerciseInfoTitle", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Дополнительная информация";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoInputDataLines", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const inputData = container.getElementsByClassName("groupval")[0];

  const actualVarNumber = inputData.getElementsByTagName("DIV")[0].textContent;
  const expectedVarNumber = "Номер варианта:1";

  const actualFileName = inputData.getElementsByTagName("DIV")[1].textContent;
  const expectedFileName = "Название файла:POTS_6hours.dat";

  expect(actualVarNumber).toBe(expectedVarNumber);
  expect(actualFileName).toBe(expectedFileName);
});

it("shouldRenderExerciseInfoSatelliteNumbersLines", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const satelliteNumbersGroup = container.getElementsByClassName("groupval")[1];

  const actualSatellite1NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[0].textContent;
  const expectedSatellite1NumberLine = "Номер первого спутника:7";

  const actualSatellite2NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[1].textContent;
  const expectedSatellite2NumberLine = "Номер второго спутника:14";

  const actualSatellite3NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[2].textContent;
  const expectedSatellite3NumberLine = "Номер третьего спутника:21";

  expect(actualSatellite1NumberLine).toBe(expectedSatellite1NumberLine);
  expect(actualSatellite2NumberLine).toBe(expectedSatellite2NumberLine);
  expect(actualSatellite3NumberLine).toBe(expectedSatellite3NumberLine);
});

it("shouldRenderExerciseInfoSatellite1P1", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[2].textContent;
  const expected = "P1 первого спутника:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite1P2", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[3].textContent;
  const expected = "P2 первого спутника:6, 7, 8, 9, 10, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2P1", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[4].textContent;
  const expected = "P1 второго спутника:11, 12, 13, 14, 15, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2P2", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[5].textContent;
  const expected = "P2 второго спутника:16, 17, 18, 19, 20, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3P1", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[6].textContent;
  const expected = "P1 третьего спутника:21, 22, 23, 24, 25, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3P2", () => {
  const satellite1Data = {
    number: 7,
    p1: ["1", "2", "3", "4", "5"],
    p2: ["6", "7", "8", "9", "10"]
  };

  const satellite2Data = {
    number: 14,
    p1: ["11", "12", "13", "14", "15"],
    p2: ["16", "17", "18", "19", "20"]
  };

  const satellite3Data = {
    number: 21,
    p1: ["21", "22", "23", "24", "25"],
    p2: ["26", "27", "28", "29", "30"]
  };

  render(
    <Var1ExerciseInfo
      varNumber="1"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[7].textContent;
  const expected = "P2 третьего спутника:26, 27, 28, 29, 30, ";
  expect(actual).toBe(expected);
});