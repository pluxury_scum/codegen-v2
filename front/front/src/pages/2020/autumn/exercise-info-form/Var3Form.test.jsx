import { render, unmountComponentAtNode } from "react-dom";
import Var3Form from "./Var3Form";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldReturnVar3FormTitle", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Введите координаты города и координаты точек для интерполяции";
  expect(actual).toBe(expected);
});

it("shouldReturnVar3FormLatInput", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[0].textContent;
  const expected = "Широта города";
  expect(actual).toBe(expected)
});

it("shouldReturnVar3FormLonInput", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[1].textContent;
  const expected = "Долгота города";
  expect(actual).toBe(expected)
});

it("shouldReturnVar3FormLat1Input", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[2].textContent;
  const expected = "Меньшая широта точек для интерполяции";
  expect(actual).toBe(expected)
});

it("shouldReturnVar3FormLat2Input", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[3].textContent;
  const expected = "Бóльшая широта точек для интерполяции";
  expect(actual).toBe(expected)
});

it("shouldReturnVar3FormLon1Input", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[4].textContent;
  const expected = "Меньшая долгота точек для интерполяции";
  expect(actual).toBe(expected)
});

it("shouldReturnVar3FormLon2Input", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[5].textContent;
  const expected = "Бóльшая долгота точек для интерполяции";
  expect(actual).toBe(expected)
});

it("shouldReturnVar3FormSatelliteNumber", () => {
  render(<Var3Form />, container);
  const actual = container.getElementsByTagName("LABEL")[6].textContent;
  const expected = "Номер спутника для сбора времени GPS";
  expect(actual).toBe(expected)
});