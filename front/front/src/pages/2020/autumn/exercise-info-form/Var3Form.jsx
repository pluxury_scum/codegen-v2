const Var3Form = () => {
  return (
    <form action="exercise-info" method="get">
      <input type="hidden" name="var" value="3" />
      <div className="ttl">Введите координаты города и координаты точек для интерполяции</div>
      <div>
        <label htmlFor="lat">Широта города</label>
        <input name="lat" type="text" id="lat" placeholder="Введите текст" />
      </div>
      <div>
        <label htmlFor="lon">Долгота города</label>
        <input name="lon" type="text" id="lon" placeholder="Введите текст" />
      </div>
      <div>
        <label htmlFor="lat1">Меньшая широта точек для интерполяции</label>
        <input name="lat1" type="text" id="lat1" placeholder="Введите текст" />
      </div>
      <div>
        <label htmlFor="lat2">Бóльшая широта точек для интерполяции</label>
        <input name="lat2" type="text" id="lat2" placeholder="Введите текст" />
      </div>
      <div>
        <label htmlFor="lon1">Меньшая долгота точек для интерполяции</label>
        <input name="lon1" type="text" id="lon1" placeholder="Введите текст" />
      </div>
      <div>
        <label htmlFor="lon2">Бóльшая долгота точек для интерполяции</label>
        <input name="lon2" type="text" id="lon2" placeholder="Введите текст" />
      </div>
      <div>
        <label htmlFor="satnum">Номер спутника для сбора времени GPS</label>
        <input name="satnum" type="text" id="satnum" placeholder="Введите текст" />
      </div>
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
}

export default Var3Form;