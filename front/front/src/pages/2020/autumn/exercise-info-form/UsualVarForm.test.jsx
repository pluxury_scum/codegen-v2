import { render, unmountComponentAtNode } from "react-dom";
import UsualVarForm from "./UsualVarForm";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldReturnUsualVarForm", () => {
  const varNumber = 7;
  render(<UsualVarForm varNumber={varNumber} />, container);

  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Введите название файла наблюдений";
  expect(actual).toBe(expected);
});