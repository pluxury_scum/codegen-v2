const JavaCode = props => {
  return (
    <span>
      <a href={props.path + "Main.java"} download="Main.java">
        <button type="submit">Загрузить Main.java</button>
      </a>
      <a href={props.path + "GraphDrawer.java"} download="GraphDrawer.java">
        <button type="submit">Загрузить GraphDrawer.java</button>
      </a>
    </span>
  );
}

export default JavaCode;