import { render, unmountComponentAtNode } from "react-dom";
import TableHeader from "./TableHeader";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFirstHeader", () => {
  render(<TableHeader />, container);

  const actual = container.getElementsByTagName("TH")[1].textContent;
  const expected = "Вариант 1";
  expect(actual).toBe(expected);
});

it("shouldRenderSecondHeader", () => {
  render(<TableHeader />, container);

  const actual = container.getElementsByTagName("TH")[2].textContent;
  const expected = "Вариант 2";
  expect(actual).toBe(expected);
});

it("shouldRenderThirdHeader", () => {
  render(<TableHeader />, container);

  const actual = container.getElementsByTagName("TH")[3].textContent;
  const expected = "Вариант 3";
  expect(actual).toBe(expected);
});